<?php

namespace Drupal\taxonomy_bulk_actions;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides \Drupal\taxonomy_bulk_actions\TaxonomyBulkActionsManagerBase.
 *
 * This is a helper class which makes it easier for other developers to
 * implement Taxonomy bulk actions plugins in their own modules.
 */
abstract class TaxonomyBulkActionsManagerBase extends PluginBase implements TaxonomyBulkActionsInterface {
  use StringTranslationTrait;

  /**
   * Provide the vids concerned by the Taxonomy Bulk Action.
   *
   * @return array
   *   An array of concerned vocabularies ids.
   */
  public function vids() {
    return $this->pluginDefinition['vids'];
  }

  /**
   * Provide the description of the Taxonomy Bulk Action.
   *
   * @return \Drupal\Core\Annotation\Translation
   *   taxonomy bulk action translatable description.
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Manage access logic to the taxonomy bulk action, return TRUE if the.
   *
   * Action should be accessible.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   Current authenticated user account object.
   *
   * @return bool
   *   A boolean value indicating within the access is accessible or not.
   */
  public function access(AccountProxyInterface $account) {
    return TRUE;
  }

  /**
   * Implementation of action logic that should be executed on set of selected.
   *
   * Term.
   *
   * @param array $terms
   *   An array of terms objects that the action will be applied on.
   */
  public function executeMultiple(array $terms) {
    foreach ($terms as $term) {
      $this->execute($term);
    }
  }

  /**
   * Add the action finish message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translatable action end message.
   */
  public function actionFinishedMessage() {
    $description = $this->description();
    return $this->t('The action "@description" execution has been finished.', ['@description' => $description]);
  }

}
