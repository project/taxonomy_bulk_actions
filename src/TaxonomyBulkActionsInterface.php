<?php

namespace Drupal\taxonomy_bulk_actions;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\taxonomy\TermInterface;

/**
 * An interface for all Taxonomy Bulk Actions type plugins.
 */
interface TaxonomyBulkActionsInterface {

  /**
   * Provide the vids concerned by the Taxonomy Bulk Action.
   *
   * @return array
   *   An array of concerned vocabularies ids.
   */
  public function vids();

  /**
   * Provide the description of the Taxonomy Bulk Action.
   *
   * @return \Drupal\Core\Annotation\Translation
   *   taxonomy bulk action translatable description.
   */
  public function description();

  /**
   * Implementation of action logic that should be executed on each selected.
   *
   * Term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   Term object that the action will be applied on.
   */
  public function execute(TermInterface $term);

  /**
   * Implementation of action logic that should be executed on set of selected.
   *
   * Term.
   *
   * @param array $terms
   *   An array of terms objects that the action will be applied on.
   */
  public function executeMultiple(array $terms);

  /**
   * Manage access to the taxonomy bulk action, return TRUE if the action is.
   *
   * Accessible.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   Current authenticated user account object.
   *
   * @return bool
   *   A boolean value indicating within the access is accessible or not.
   */
  public function access(AccountProxyInterface $account);

  /**
   * Add the action finish message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translatable action end message.
   */
  public function actionFinishedMessage();

}
