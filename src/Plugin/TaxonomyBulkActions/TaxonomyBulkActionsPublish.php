<?php

namespace Drupal\taxonomy_bulk_actions\Plugin\TaxonomyBulkActions;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy_bulk_actions\TaxonomyBulkActionsManagerBase;

/**
 * Publish taxonomy term action.
 *
 * @TaxonomyBulkActions(
 *   id="taxonomy_bulk_action_publish",
 *   description="Publish selected terms"
 * )
 */
class TaxonomyBulkActionsPublish extends TaxonomyBulkActionsManagerBase {

  /**
   * Manage access to the taxonomy bulk action, return TRUE if the action is.
   *
   * Accessible.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   Current authenticated user account object.
   *
   * @return bool
   *   A boolean value indicating within the access is accessible or not.
   */
  public function access(AccountProxyInterface $account) {
    return $account->hasPermission('administer taxonomy');
  }

  /**
   * Implementation of action logic that should be executed on each selected.
   *
   * Term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   Term object that the action will be applied on.
   */
  public function execute(TermInterface $term) {
    $term->set('status', 1);
    $term->save();
  }

  /**
   * Add the action finish message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translatable action end message.
   */
  public function actionFinishedMessage() {
    return $this->t('Publishing selected terms action has been finished');
  }

}
