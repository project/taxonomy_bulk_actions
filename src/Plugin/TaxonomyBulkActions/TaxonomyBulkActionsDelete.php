<?php

namespace Drupal\taxonomy_bulk_actions\Plugin\TaxonomyBulkActions;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy_bulk_actions\TaxonomyBulkActionsManagerBase;

/**
 * Delete taxonomy term action.
 *
 * @TaxonomyBulkActions(
 *   id="taxonomy_bulk_action_delete",
 *   description="Delete selected terms"
 * )
 */
class TaxonomyBulkActionsDelete extends TaxonomyBulkActionsManagerBase {

  /**
   * Manage access to the taxonomy bulk action, return TRUE if the action is.
   *
   * Accessible.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   Current authenticated user account object.
   *
   * @return bool
   *   A boolean value indicating within the access is accessible or not.
   */
  public function access(AccountProxyInterface $account) {
    // Check for delete permission for the current vocabulary.
    if ($vocabulary = \Drupal::routeMatch()->getParameter('taxonomy_vocabulary')) {
      $vid = $vocabulary->id();
      $delete_permission = "delete terms in {$vid}";
      if ($account->hasPermission($delete_permission)) {
        return TRUE;
      }
    }
    // Check for the 'Administer vocabularies and terms' permission.
    return $account->hasPermission('administer taxonomy');
  }

  /**
   * Implementation of action logic that should be executed on each selected.
   *
   * Term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   Term object that the action will be applied on.
   */
  public function execute(TermInterface $term) {
    $term->delete();
  }

  /**
   * Add the action finish message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translatable action end message.
   */
  public function actionFinishedMessage() {
    return $this->t('Deleting selected terms action has been finished');
  }

}
