<?php

namespace Drupal\taxonomy_bulk_actions\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Taxonomy Bulk Actions annotation object.
 *
 * @Annotation
 */
class TaxonomyBulkActions extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The action translatable description, this will be appeared on the.
   *
   * Taxonomy bulk actions dropdown select.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The concerned vocabulary IDs, if empty array or omitted that means the.
   *
   * Action concern all vocabularies terms.
   *
   * @var array
   */
  public $vids;

}
