(function($, Drupal) {
  Drupal.behaviors.ux_guide = {
    attach: function (context, settings) {
      $(window).on('scroll', function () {
        windowScrollTop = $(window).scrollTop();
        if (windowScrollTop >= 100) {
          $('#taxonomy-bulk-actions-container').addClass('tba-sticky')
        }
        else {
          $('#taxonomy-bulk-actions-container').removeClass('tba-sticky');
        }
      })
      $('#taxonomy-bulk-actions-select-all').on('change', function (e) {
        $selectAllChecked = $(this).prop('checked');
        if ($selectAllChecked) {
          $('.term-selector').each(function () {
            $(this).prop('checked', true);
          });
        }
        else {
          $('.term-selector').each(function () {
            $(this).prop('checked', false);
          });
        }
      })
    }
  };
})(jQuery, Drupal);
