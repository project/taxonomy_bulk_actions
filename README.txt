INTRODUCTION
------------

Taxonomy Bulk Actions module enhance Drupal default taxonomy terms listing
with a powerful extensible bulk action system based on a new
TaxonomyBulkActionsManager plugin type, For more details about how to use
TaxonomyBulkActionsManager plugin check the module download page example on
Drupal.org: https://www.drupal.org/project/taxonomy_bulk_actions


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no settings. There is no configuration. When
enabled, you can visit your vocabulary terms listing and you will find a
dropdown selector for possible actions to apply by term.
